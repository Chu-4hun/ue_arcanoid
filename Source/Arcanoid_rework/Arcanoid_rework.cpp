// Copyright Epic Games, Inc. All Rights Reserved.

#include "Arcanoid_rework.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Arcanoid_rework, "Arcanoid_rework" );
