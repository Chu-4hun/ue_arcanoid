// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Arcanoid_reworkGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ARCANOID_REWORK_API AArcanoid_reworkGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
