// Fill out your copyright notice in the Description page of Project Settings.


#include "Arcanoid_rework/Public/Brick.h"


// Sets default values
ABrick::ABrick()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABrick::BeginPlay()
{
	Super::BeginPlay();
}

void ABrick::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void ABrick::UpdateState_Implementation()
{
	UE_LOG(LogTemp, Display, TEXT("Default brick update"));
	if (Health <= 0)
	{
		this->Destroy();
	}
}

void ABrick::DealDamage_Implementation(int Amount, APawn* InstigatorPawn)
{
	IDamageable::DealDamage_Implementation(Amount, InstigatorPawn);
	InstigatorPlayer = InstigatorPawn;
	if (Health >= 0)
	{
		Health -= 1;
		UpdateState();
	}
	else
	{
		this->Destroy();
	}
}
