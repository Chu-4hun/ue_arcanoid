// Fill out your copyright notice in the Description page of Project Settings.


#include "Arcanoid_rework/Public/Spawner.h"

#include "EnvironmentQuery/EnvQueryTypes.h"


// Sets default values
ASpawner::ASpawner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	DefaultSceneRoot = CreateDefaultSubobject<USceneComponent>("Root");
	DefaultSceneRoot->SetupAttachment(GetRootComponent());
}

// Called when the game starts or when spawned
// FIXME DEAD CODE i tried porting it (to c++) with more variety in generation but something went wrong 
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	FActorSpawnParameters Params;
	Params.bDeferConstruction = true;
	// We defer construction so that we set ParentComponentActor prior to component registration so they appear selected
	Params.bAllowDuringConstructionScript = true;
	FVector Location = DefaultSceneRoot->GetComponentLocation();
	const FRotator Rotation = DefaultSceneRoot->GetComponentRotation();
	for (int i = 0; i <= MaxBlocksInWidth; i++)
	{
		for (int j = 0; j <= FMath::RandRange(0, MaxBlocksInHeight); j++)
		{
			const int RandomNumber = rand() % 100;
			// i tried ranged switch statements but https://stackoverflow.com/questions/9432226/how-do-i-select-a-range-of-values-in-a-switch-statement
			// Some compilers support case ranges like case x ... y as an _extension_ to the C++ language.
			Location = FVector(Location.X + (j * VerticalOffset), Location.Y + (i * HorizontalOffset), Location.Z);

			if (RandomNumber >= 80)
			{
				UClass* SpawnClass = BrickVariations[3] == nullptr
					                     ? BrickVariations[0]->StaticClass()
					                     : BrickVariations.Last()->StaticClass();
				GetWorld()->SpawnActor<AActor>(BrickVariations[3],Location, Rotation, Params);
				break;
			}
			if (RandomNumber >= 60)
			{
				UClass* SpawnClass = BrickVariations[2] == nullptr
										 ? BrickVariations[0]->StaticClass()
										 : BrickVariations.Last()->StaticClass();
				GetWorld()->SpawnActor<AActor>(BrickVariations[2], Location, Rotation, Params);
				break;
			}
			if (RandomNumber >= 50)
			{
				UClass* SpawnClass = BrickVariations[1] == nullptr
										 ? BrickVariations[0]->StaticClass()
										 : BrickVariations.Last()->StaticClass();
				GetWorld()->SpawnActor<AActor>(BrickVariations[1], Location, Rotation, Params);
				break;
			}
			if (RandomNumber < 50)
			{
				UClass* SpawnClass = BrickVariations[0]; 
				GetWorld()->SpawnActor<AActor>(BrickVariations[0], Location, Rotation, Params);
				break;
			}
		}
	}
}
