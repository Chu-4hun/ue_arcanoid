// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/Damageable.h"
#include "Brick.generated.h"

UCLASS()
class ARCANOID_REWORK_API ABrick : public AActor, public IDamageable
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABrick();
	UPROPERTY(EditAnywhere, Meta = (Base))
	int Health;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite)
	APawn* InstigatorPlayer;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// I want easily create modifiers bricks in Blueprints
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void UpdateState();
	
	virtual void DealDamage_Implementation(int Amount, APawn* InstigatorPawn) override;
};
